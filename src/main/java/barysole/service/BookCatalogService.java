package barysole.service;

import barysole.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BookCatalogService {

    Page<Book> getAllBooks(Pageable pageable);

    Book getBookById(String id);

    Book saveBook(Book book);

    void deleteBookById(String id);

}
