package barysole.dto;

import barysole.enums.BookStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookDTO {

    @Schema(hidden = true)
    private String id;

    private String title;

    private String author;

    private BookStatus bookStatus;

    private String borrowedBy;

    private String reservedBy;

}
