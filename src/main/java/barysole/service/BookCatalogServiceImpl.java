package barysole.service;

import barysole.entity.Book;
import barysole.enums.BookStatus;
import barysole.exceptions.NotFoundException;
import barysole.repository.BookRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class BookCatalogServiceImpl implements BookCatalogService {

    private final BookRepository bookRepository;

    public BookCatalogServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Page<Book> getAllBooks(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    @Override
    public Book getBookById(String id) {
        return bookRepository.findById(id).orElseThrow(() -> new NotFoundException("Book with id \"" + id + "\" not found!"));
    }

    @Override
    public Book saveBook(Book book) {
        if (book.getBookStatus() == null) {
            book.setBookStatus(BookStatus.AVAILABLE);
        }
        return bookRepository.save(book);
    }

    @Override
    public void deleteBookById(String id) {
        bookRepository.deleteById(id);
    }

}
