package barysole.controller;

import barysole.dto.BookDTO;
import barysole.entity.Book;
import barysole.exceptions.NotFoundException;
import barysole.service.BookCatalogService;
import barysole.transformer.UniversalTransformer;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/bookCatalog")
public class BookCatalogController extends GenericController {

    private final BookCatalogService bookService;

    public BookCatalogController(BookCatalogService bookService, UniversalTransformer universalTransformer) {
        super(universalTransformer);
        this.bookService = bookService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Books successfully retrieved"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public Page<BookDTO> getBookList(@PageableDefault(size = 1000) Pageable pageable) {
        return transformPage(bookService.getAllBooks(pageable), pageable, BookDTO.class);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book successfully retrieved"),
            @ApiResponse(responseCode = "404", description = "Book not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public BookDTO getBook(@PathVariable String id) {
        try {
            return transform(bookService.getBookById(id), BookDTO.class);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.PUT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book is successfully saved/updated"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public BookDTO saveBook(@RequestBody BookDTO book) {
            return transform(bookService.saveBook(transform(book, Book.class)), BookDTO.class);
    }

    @RequestMapping(value = "/saveAll", method = RequestMethod.PUT)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Books successfully saved/updated"),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public List<BookDTO> saveAllBooks(@RequestBody List<BookDTO> bookList) {
        List<BookDTO> returnList = new ArrayList<>(bookList.size());
        for (BookDTO book :
                bookList) {
            returnList.add(transform(bookService.saveBook(transform(book, Book.class)), BookDTO.class));
        }
        return returnList;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book successfully deleted", content = @Content()),
            @ApiResponse(responseCode = "404", description = "Book not found in db", content = @Content()),
            @ApiResponse(responseCode = "500", description = "Internal service exception happened", content = @Content())
    })
    public void deleteBook(@PathVariable String id) {
        try {
            bookService.deleteBookById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
