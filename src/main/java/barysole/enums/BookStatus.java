package barysole.enums;

public enum BookStatus {
    AVAILABLE,
    BORROWED,
    NOT_AVAILABLE,
    RESERVED
}
