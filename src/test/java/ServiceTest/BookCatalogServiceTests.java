package ServiceTest;

import barysole.SwaCatalogServiceApplication;
import barysole.entity.Book;
import barysole.enums.BookStatus;
import barysole.exceptions.NotFoundException;
import barysole.service.BookCatalogService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SwaCatalogServiceApplication.class)
public class BookCatalogServiceTests {

    @Autowired
    private BookCatalogService bookCatalogService;

    private static Book createTestBook() {
        Book testBook = new Book();
        testBook.setTitle("ipsum officia");
        testBook.setAuthor("Holman Hyde");
        return testBook;
    }

    @Test
    public void create_and_get_book_should_execute_successfully() {
        Book newBook = createTestBook();
        newBook = bookCatalogService.saveBook(newBook);
        Assertions.assertEquals(newBook, bookCatalogService.getBookById(newBook.getId()));
    }

    @Test
    public void created_book_without_status_should_be_available() {
        Book newBook = createTestBook();
        newBook = bookCatalogService.saveBook(newBook);
        Assertions.assertEquals(BookStatus.AVAILABLE, bookCatalogService.getBookById(newBook.getId()).getBookStatus());
    }

    @Test
    public void get_nonexistent_book_should_throw_not_found_exception() {
        Assertions.assertThrows(NotFoundException.class, () -> bookCatalogService.getBookById("123d"));
    }

    @Test
    public void delete_existent_book_should_execute_successfully() {
        Book newBook = bookCatalogService.saveBook(createTestBook());
        bookCatalogService.deleteBookById(newBook.getId());
        Assertions.assertThrows(NotFoundException.class, () -> bookCatalogService.getBookById(newBook.getId()));
    }

}
